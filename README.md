# YouTube/ニコニコ動画 ビューワー
[![pipeline status](https://gitlab.com/kasmide/videoviewer/badges/master/pipeline.svg)](https://gitlab.com/kasmide/videoviewer/commits/master)
## 機能
* 動画の検索
* 動画を埋め込みプレイヤーで再生
* 動画をブックマーク

## ライセンス
Apache License 2.0